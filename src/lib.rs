#[cfg(test)]
#[macro_use]
extern crate approx;

pub mod library;
pub mod analysis;

pub use library::soundfile::SoundFile;

mod log;
pub use log::log_write_file;
pub use log::visualisation;