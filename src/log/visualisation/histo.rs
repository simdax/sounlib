use plotters::prelude::*;

pub fn histo(path: &str, data: &[f32], min: f32, max: f32) -> Result<(), Box<dyn std::error::Error>>
{
    let root = BitMapBackend::new(path, (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;
    let mut chart = ChartBuilder::on(&root)
        .x_label_area_size(35)
        .y_label_area_size(40)
        .margin(5)
        .build_cartesian_2d((0..data.len()).into_segmented(), min..max)?;
    chart
        .configure_mesh()
        .disable_x_mesh()
        .disable_y_mesh()
        .bold_line_style(&WHITE.mix(0.3))
        .y_desc("Count")
        .x_desc("Bucket")
        .axis_desc_style(("sans-serif", 15))
        .draw()?;
    chart.draw_series(
        Histogram::vertical(&chart)
            .style(RED.mix(0.5).filled())
            .data(data.iter().enumerate().map(|(i, x)| (i, *x))),
    )?;
    root.present().expect(&format!(
        "Unable to write result to file, please make sure '{}' exists under current dir",
        path
    ));
    Ok(())
}

#[test]
fn test_histo() {
    let data = [0., 1.2, -2., 3., -4.];
    histo("histo.png", &data, -5., 5.).unwrap()
}
