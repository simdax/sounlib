use std::fs::OpenOptions;
use std::io::Write;
use std::slice;

pub mod visualisation;

fn write_data<T>(mut file: std::fs::File, data: &[T]) {
    let slice_u8: &[u8] = unsafe {
        slice::from_raw_parts(
            data[..].as_ptr() as *const u8,
            data.len() * std::mem::size_of::<T>(),
        )
    };
    file.write_all(slice_u8).unwrap();
}

pub fn log_write_file<T>(path: &str, data: &[T]) {
    let file = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .open(path)
        .unwrap();
    write_data(file, data);
}

pub fn log_append_file<T>(path: &str, data: &[T]) {
    let file = OpenOptions::new()
        .create(true)
        .append(true)
        .write(true)
        .open(path)
        .unwrap();
    write_data(file, data);
}
