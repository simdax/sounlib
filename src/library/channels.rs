#[derive(Clone)]
pub struct Channels<T: Clone> {
    pub samples: Vec<T>,
    pub nb: u8,
}

impl<T: Clone> Channels<T> {
    pub fn get_channel(&self, nb: u8) -> impl Iterator<Item = T> {
        let samples = self.samples.clone();
        samples
            .into_iter()
            .skip(nb as usize)
            .step_by(self.nb as usize)
    }
}

impl<T: Clone> IntoIterator for Channels<T> {
    type Item = T;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.samples.into_iter()
    }
}

#[test]
fn test_iter_channels() {
    let channels = Channels {
        samples: vec![0, 1, 2, 3],
        nb: 2,
    };
    let mut iter = channels.clone().into_iter();
    assert_eq!(iter.next(), Some(0));
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(3));
    let mut iter = channels.into_iter();
    assert_eq!(iter.next(), Some(0));
    assert_eq!(iter.next(), Some(1));
    assert_eq!(iter.next(), Some(2));
    assert_eq!(iter.next(), Some(3));
}

#[test]
fn test_iter_channels_separate() {
    let channels = Channels {
        samples: vec![0, 1, 2, 3],
        nb: 2,
    };
    let mut chan0 = channels.get_channel(0);
    let mut chan1 = channels.get_channel(1);

    assert_eq!(chan0.next(), Some(0));
    assert_eq!(chan0.next(), Some(2));
    assert_eq!(chan1.next(), Some(1));
    assert_eq!(chan1.next(), Some(3));
}

#[test]
fn test_load_samples() {
    let vec = vec![0, 1, 2, 3];
    let mut iter = vec.iter().skip(2).cycle();
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&3));
}
