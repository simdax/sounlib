use super::load;
use std::io::{BufRead, Read};

#[derive(Default, Clone, Copy)]
pub struct Infos {
    pub nb_channels: u8,
    pub sample_rate: u16,
    sample_size: u8,
    interleaved: bool,
}

impl Infos {
    pub fn new_from_wav(path: &str) -> (Self, Vec<u8>) {
        let file = load::open_file(path).unwrap();
        let header = load::WavHeader::load(&file).unwrap();
        let reader = &mut std::io::BufReader::new(&file);
        let mut iterator = reader.bytes().map(|x| x.unwrap());
        let offset = load::find_in_file(&mut iterator, "data");
        reader.consume(offset);
        let mut buffer = vec![];
        let _size = reader.read_to_end(&mut buffer);
        //TODO: assert size
        // std::mem::size_of::<load::WavHeader>() as u8 + offset as u8,
        (
            Infos {
                nb_channels: header.num_channels as u8,
                sample_size: header.bits_per_sample as u8,
                sample_rate: header.sample_rate as u16,
                interleaved: true,
            },
            buffer,
        )
    }
}

#[test]
pub fn test_load_infos_booth() {
    let (infos, data) = Infos::new_from_wav("booth-ambient.wav");
    assert_eq!(infos.nb_channels, 2);
    assert_eq!(infos.sample_size, 16);
    assert_eq!(data[0], 0xb0);
    assert_eq!(data[1], 0xff);
    assert_eq!(data[2], 0x10);
    assert_eq!(data[3], 0x00);
}

#[test]
pub fn test_load_infos_boing() {
    let (infos, data) = Infos::new_from_wav("boing.wav");
    assert_eq!(infos.nb_channels, 2);
    assert_eq!(infos.sample_size, 16);
    assert_eq!(data[0], 0x00);
    assert_eq!(data[1], 0x00);
    assert_eq!(data[2], 0x00);
    assert_eq!(data[3], 0x00);
    assert_eq!(data[4], 0xfe);
    assert_eq!(data[5], 0xff);
}
