pub fn cast_toi16(bytes: &[u8]) -> Vec<i16> {
    bytes
        .chunks_exact(2)
        .into_iter()
        .map(|a| i16::from_ne_bytes([a[0], a[1]]))
        .collect()
}

#[test]
fn cast_test() {
    let title = cast_toi16(&[0, 1, 2, 3]);
    assert_eq!(title[0], 0x0100);
    assert_eq!(title[1], 0x0302);
}
