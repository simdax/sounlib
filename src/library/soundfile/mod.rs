mod cast;
use super::{channels::Channels, infos::Infos};

pub struct SoundFile<T: Clone> {
    pub infos: Infos,
    pub channels: Channels<T>,
}

impl SoundFile<i16> {
    pub fn new(path: &str) -> SoundFile<i16> {
        let (infos, data) = Infos::new_from_wav(path);
        let samples = cast::cast_toi16(&data);
        SoundFile {
            infos,
            channels: Channels {
                samples,
                nb: infos.nb_channels,
            },
        }
    }
}

impl<T: Clone> IntoIterator for SoundFile<T> {
    type Item = T;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.channels.samples.into_iter()
    }
}

#[test]
fn test_soundfile() {
    let sndfile = SoundFile::new("booth-ambient.wav");
    let samples: Vec<i16> = sndfile.into_iter().take(2).collect();
    assert_eq!(samples[0], -80);
    assert_eq!(samples[1], 0x0010);
}

#[test]
fn write_wav() {
    let sndfile = SoundFile::new("boing.wav");
    let samples: Vec<i16> = sndfile.into_iter().collect();
    crate::log_write_file("file.raw", &samples);
}
