use std::{
    io::{Error, Read},
    mem,
};

#[derive(Debug, Default)]
#[repr(C)]
pub struct WavHeader {
    chunk_id: u32,
    chunk_size: u32,
    pub format: u32,
    sub_chunk1_id: u32,
    sub_chunk1_size: u32,
    pub audio_format: u16,
    pub num_channels: u16,
    pub sample_rate: u32,
    byte_rate: u32,
    block_align: u16,
    pub bits_per_sample: u16,
}

impl WavHeader {
    pub fn load(mut reader: impl Read) -> Result<Self, Error> {
        let mut header = WavHeader::default();

        unsafe {
            let buffer: &mut [u8] = std::slice::from_raw_parts_mut(
                &mut header as *mut WavHeader as *mut u8,
                mem::size_of::<WavHeader>(),
            );
            reader.read_exact(buffer)?;
            Ok(header)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::io::Cursor;

    const DATA: [u8; std::mem::size_of::<WavHeader>()] = [
        0x52, 0x49, 0x46, 0x46, 0x74, 0x75, 0x0D, 0x00, 0x57, 0x41, 0x56, 0x45, 0x66, 0x6D, 0x74,
        0x20, 0x10, 0x00, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x44, 0xAC, 0x00, 0x00, 0x10, 0xB1,
        0x02, 0x00, 0x04, 0x00, 0x10, 0x00,
    ];

    #[test]
    fn test_wav_header() {
        let file = Cursor::new(DATA);
        let wav_header = WavHeader::load(file).unwrap();
        assert_eq!(wav_header.chunk_id, 0x46464952);
        assert_eq!(wav_header.chunk_size, 0x000d7574);
        assert_eq!(wav_header.format, 0x45564157);
        assert_eq!(wav_header.sub_chunk1_id, 0x20746d66);
        assert_eq!(wav_header.sub_chunk1_size, 16);
        assert_eq!(wav_header.audio_format, 1);
        assert_eq!(wav_header.num_channels, 2);
        assert_eq!(wav_header.sample_rate, 44100);
        assert_eq!(
            wav_header.byte_rate,
            wav_header.sample_rate * wav_header.block_align as u32
        );
        assert_eq!(
            wav_header.block_align,
            wav_header.num_channels * (wav_header.bits_per_sample / 8)
        );
        assert_eq!(wav_header.bits_per_sample, 16);
    }
}
