mod wavheader;

pub use wavheader::*;

pub fn open_file(path: &str) -> std::io::Result<std::fs::File> {
    let root_path = std::env::vars()
        .find(|x| x.0 == "CARGO_MANIFEST_DIR")
        .unwrap()
        .1;
    let path = std::path::Path::new(&root_path).join("data").join(path);
    std::fs::File::open(path)
}

pub fn find_in_file<T: Iterator<Item = u8>>(iterator: &mut T, str: &str) -> usize {
    let mut position = 0;

    'outer: loop {
        'inner: loop {
            for d in str.bytes() {
                let n = iterator.next().unwrap();
                position += 1;
                if d != n {
                    break 'inner;
                }
            }
            break 'outer;
        }
    }
    position
}

#[cfg(test)]
mod test_file {
    use std::io::Read;
    use super::*;
    use wavheader::WavHeader;

    #[test]
    fn test_open_file() {
        open_file("booth-ambient.wav").unwrap();
    }

    #[test]
    fn test_open_wavfile() {
        let file = open_file("booth-ambient.wav").unwrap();
        let header = WavHeader::load(file).unwrap();
        assert_eq!(header.sample_rate, 44100);
        let file = open_file("boing.wav").unwrap();
        let header = WavHeader::load(file).unwrap();
        assert_eq!(header.sample_rate, 44100);
    }

    #[test]
    fn test_offset() {
        let file = open_file("booth-ambient.wav").unwrap();
        assert_eq!(find_in_file(&mut "OK LES GARS".bytes(), "LES"), 6);
        assert_eq!(
            find_in_file(&mut file.bytes().map(|x| x.unwrap()), "data"),
            40
        );
    }
}
