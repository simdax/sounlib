mod complex;

use complex::Complex;

pub fn dft(signal: &[f32], sr: &u16) -> Vec<f32> {
    let step = std::f32::consts::TAU / *sr as f32;
    (20..2000)
        .into_iter()
        .map(|freq| {
            signal
                .iter().enumerate()
                .map(|(i, x)| Complex::from_polar_mag(step * freq as f32 * i as f32, *x))
                .sum::<Complex>().real / signal.len() as f32
        })
        .collect()
}
