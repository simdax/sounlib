use std::ops::Add;
use std::ops::Mul;

#[derive(Debug, Default, PartialEq)]
pub struct Complex {
    pub real: f32,
    pub im: f32,
}

impl Complex {
    pub fn from_polar_mag(angle: f32, mag: f32) -> Self {
        Complex {
            real: angle.cos() * mag,
            im: angle.sin() * mag,
        }
    }

    fn mag(&self) -> f32 {
        self.real * self.real + self.im * self.im
    }
}

impl Add for Complex {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Complex {
            real: self.real + rhs.real,
            im: self.im + rhs.im,
        }
    }
}

impl Mul for Complex {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self {
        Complex {
            real: self.real * rhs.real - self.im * rhs.im,
            im: self.real * rhs.im + self.im * rhs.real,
        }
    }
}

impl std::iter::Sum for Complex {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        iter.fold(Complex::default(), Add::add)
    }
}

impl std::iter::Product for Complex {
    fn product<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        iter.fold(Complex { real: 1., im: 1. }, Mul::mul)
    }
}

#[test]
fn test_complex() {
    for (angle, result) in &[
        (0., (2., 0.)),
        (std::f32::consts::FRAC_PI_2, (0., 2.)),
        (std::f32::consts::PI, (-2., 0.)),
        (
            std::f32::consts::PI + std::f32::consts::FRAC_PI_2,
            (0., -2.),
        ),
        (std::f32::consts::TAU, (2., 0.)),
    ] {
        let complex = Complex::from_polar_mag(*angle, 2.);
        assert_relative_eq!(complex.real, result.0, epsilon = 0.000001);
        assert_relative_eq!(complex.im, result.1, epsilon = 0.000001);
    }
    for i in 0..10 {
        let mag = i as f32 * 0.23;
        let angle = i as f32 * 0.42;
        let complex = Complex::from_polar_mag(angle, mag);
        assert_relative_eq!(complex.mag(), mag * mag);
    }
}

#[test]
fn test_add() {
    assert_eq!(
        Complex { real: 1., im: 1. } + Complex { real: 1., im: 1. },
        Complex { real: 2., im: 2. }
    );
}

#[test]
fn test_mul() {
    assert_eq!(
        Complex::from_polar_mag(0., 1.) * Complex::from_polar_mag(std::f32::consts::PI, 2.),
        Complex { real: 2., im: 0. }
    );
}

#[test]
fn center_mass2() {
    let pts = 8;
    let mut center_orig = (0..8).into_iter().map(|x| {
        println!("{}", x);
        let angle = (std::f32::consts::TAU / pts as f32) * x as f32;
        Complex::from_polar_mag(angle, 1f32)
    });
    let mut center = center_orig.clone();
    assert_eq!(std::f32::consts::FRAC_PI_4, std::f32::consts::TAU / 8. * 1.);
    assert_eq!(center.len(), 8);
    assert_eq!(center.nth(0).unwrap(), Complex { real: 1., im: 0. });
    assert_eq!(
        center.nth(0).unwrap(),
        Complex::from_polar_mag(std::f32::consts::FRAC_PI_4, 1.)
    );
    assert_eq!(
        center.nth(0).unwrap(),
        Complex::from_polar_mag(std::f32::consts::FRAC_PI_2, 1.)
    );
    //
    let center = center_orig.sum::<Complex>();
    assert_relative_eq!(center.real, 0.);
    assert_relative_eq!(center.im, 0.);
}
