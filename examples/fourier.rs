use soundfile::SoundFile;

fn main() {
    let signal = SoundFile::new("boing.wav");
    let sr = signal.infos.sample_rate;
    let analysis = soundfile::analysis::dft(
        &signal
            .into_iter()
            .map(|x| x as f32 / std::i16::MAX as f32)
            .take(4000)
            .collect::<Vec<f32>>()[..],
        &sr,
    );
    soundfile::visualisation::histo::histo("fft.png", &analysis, 0., 1.);
    soundfile::log_write_file("analysis.raw", &analysis);
}

#[test]
fn test_dtf() {
    let signal = basic_sin(440., sr, sr);
    let sr = 44100;
    let analysis = soundfile::analysis::dft(&signal.into_iter[..4000], &sr);
}
