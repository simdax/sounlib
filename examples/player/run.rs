use cpal::traits::DeviceTrait;

pub struct Runner<'a> {
    pub device: &'a cpal::Device,
    pub config: &'a cpal::StreamConfig,
    pub sample_format: cpal::SampleFormat,
}

impl<'a> Runner<'a> {
    pub fn cue<
        OutFormat: cpal::Sample,
        InFormat: cpal::Sample,
        T: Iterator<Item = InFormat> + Send + 'static,
    >(
        &self,
        mut samples: T,
    ) -> Result<cpal::Stream, anyhow::Error> {
        Ok(self.device.build_output_stream(
            self.config,
            move |data: &mut [OutFormat], _: &cpal::OutputCallbackInfo| {
                data.fill_with(|| cpal::Sample::from::<InFormat>(&samples.next().unwrap()))
            },
            |err| eprintln!("an error occurred on stream: {}", err),
        )?)
    }
}
