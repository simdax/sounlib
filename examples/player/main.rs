mod open;
mod run;

use cpal::traits::StreamTrait;
use soundfile::library::soundfile::SoundFile;

macro_rules! play {
    ($runner:ident, $snd:ident, $($e: ident ($($f: expr)*) ),*) => {
        let s = match $runner.sample_format {
            cpal::SampleFormat::F32 => $runner.cue::<f32, _, _>($snd.clone()$(.$e($($f)*))*)?,
            cpal::SampleFormat::U16 => $runner.cue::<u16, _, _>($snd.clone()$(.$e($($f)*))*)?,
            cpal::SampleFormat::I16 => $runner.cue::<i16, _, _>($snd.clone()$(.$e($($f)*))*)?,
        };
        s.play().unwrap();
    };
}

fn main() -> anyhow::Result<()> {
    let boing = SoundFile::new("boing.wav").into_iter();
    let (device, config) = open::open()?;
    let sample_format = config.sample_format();
    let runner = run::Runner {
        device: &device,
        config: &config.into(),
        sample_format,
    };
    let volume = std::sync::Arc::new(atomic_float::AtomicF32::from(1.));
    let clone = volume.clone(); 
    play!(
        runner,
        boing,
        cycle(),
        map(move |x| (x as f32 * volume.load(std::sync::atomic::Ordering::Acquire)) as i16)
    );
    let fps = 30;
    let dur = 3000;
    for _v in 1..fps + 1 {
        clone.fetch_sub(1./fps as f32, std::sync::atomic::Ordering::Acquire);
        std::thread::sleep(std::time::Duration::from_millis(dur / fps));
    }
    Ok(())
}
